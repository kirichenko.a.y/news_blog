<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentForm;
use Illuminate\Http\Request;
use App\Models\Post;


class PostController extends Controller
{
    public function Index()
    {
        $posts = Post::orderBy("created_at", "DESC")->paginate(3);

        return view('posts.index', [
            "posts" => $posts,
        ]);
    }

    public function show($id)
    {
        $posts = Post::with("comments.user")->findOrFail($id);

        return view('posts.show', [
            "post" => $posts,
        ]);
    }

    public function comment($id, CommentForm $request)
    {
        $post = Post::findOrFail($id);

        $post->comments()->create($request->validated());

        return redirect(route("posts.show", $id));
    }
}
