<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactFormRequest;
use App\Mail\ContactForm;
use App\Models\AdminUser;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

class IndexController extends Controller
{
    public function Index()
    {
        $posts = Post::orderBy("created_at", "DESC")->paginate(3);

        return view('welcome', [
            "posts" => $posts,
        ]);
    }

    public function showContactForm()
    {
        return view("contact_form");
    }

    public function contactForm(ContactFormRequest $request)
    {
        Mail::to("test@test.com")->send(new ContactForm($request->validated()));

        return redirect(route("contacts"));
    }
}
